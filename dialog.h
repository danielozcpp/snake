#ifndef DIALOG_H
#define DIALOG_H
#include <QTimer>

#include <QDialog>
#include <QtGui>
#include <QtCore>
#include <QLinearGradient>
#include <QKeyEvent>
#include <QGraphicsItem>
#include <QPushButton>
#include <QVBoxLayout>

#include <QDebug>

#include <iostream>
#include <ostream>
#include <fstream>
#include <string>
using namespace std;

namespace Ui {
    class Dialog;
}


class Updating :public QObject, public QGraphicsPathItem
{
    Q_OBJECT
 int mx,my;  int *snake,snake_long,snake_start,snake_end;
 int last_action_x,last_action_y;
public:
   QGraphicsScene *mscene;
  QGraphicsView *mview;
    QKeyEvent *key;
    QPushButton *w_button;
    QPushButton *s_pressed;
    QPushButton *d_pressed;
    QPushButton *a_pressed;
    QVBoxLayout *lay;
    QWidget *for_button;
     QPainterPath p;
    QPixmap *pixmap;

     //this->keyPressEvent();

public:
   Updating();

public slots:
   void advance(int phase);
   void addw();
   void adds();
   void adda();
   void addd();
   void move();
   void keyPressEvent(QKeyEvent *event);
};

#endif // DIALOG_H
