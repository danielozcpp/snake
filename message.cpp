
//#include "ui_dialog_message_popup_window.h"

#include "message.h"



dialog_message_popup_window::dialog_message_popup_window(QWidget *parent) : QDialog(parent), ui(new Ui::dialog_message_popup_window)
{
    ui->setupUi(this);
    this->setWindowTitle("Information");
}

dialog_message_popup_window::~dialog_message_popup_window()
{
    delete ui;
}

void dialog_message_popup_window::mshow(QString text)
{
  ui->label->setText(text);
  ui->label->adjustSize();
  this->exec();
}


void dialog_message_popup_window::on_pushButton_clicked()
{
  this->close();
}
